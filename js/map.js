$( document ).ready(function() {

  // the body elements needs to get a fixed height so that the map can fill 100% of height
  function setBodyHeight() {
    $('body').height($(window).height() - parseInt($('body').css('padding-top')));    
  }
  
  $( window ).resize(function() {
    setBodyHeight();
  });

  setBodyHeight();
  
  var map = L.map('map').setView([49.4489, 11.0783], 10);
  
  // Standard OSM
  var osmLayer = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  });
  osmLayer.addTo(map);

  // Mapbox
  var mapBoxStreets = L.tileLayer('https://{s}.tiles.mapbox.com/v3/windwolke.kl4pokco/{z}/{x}/{y}.png');
  var mapBoxTerrain = L.tileLayer('https://{s}.tiles.mapbox.com/v3/windwolke.klaki6g9/{z}/{x}/{y}.png');

  // MapQuest
  var mapQuestSat = L.tileLayer('http://otile{s}.mqcdn.com/tiles/1.0.0/sat/{z}/{x}/{y}.jpeg', {
                      attribution: 'Tiles by <a href="http://www.mapquest.com/">MapQuest</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
                      subdomains: '1234'
                    }); 
  
  var mapQuestMap = L.tileLayer('http://otile{s}.mqcdn.com/tiles/1.0.0/map/{z}/{x}/{y}.jpeg', {
                      attribution: 'Tiles by <a href="http://www.mapquest.com/">MapQuest</a> &mdash; Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>',
                      subdomains: '1234'
                    }); 
 
  var baseMaps = {
      "Standard OSM": osmLayer,
      "MapQuest map": mapQuestMap,
      "MapQuest satellite": mapQuestSat,
      "Mapbox streets": mapBoxStreets,
      "Mapbox terrain": mapBoxTerrain
  };
  
  // Postboxes via OverPassAPI overlay
  var postboxes = new L.OverPassLayer({
    minzoom: 14,
    query: "node(BBOX)['amenity'='post_box'];out;",
  });

  // Toilets via OverPassAPI overlay
  var toilets = new L.OverPassLayer({
    minzoom: 14,
    query: "node(BBOX)['amenity'='toilets'];out;",
  });

  // farm shops via OverPassAPI overlay
  var farmshops = new L.OverPassLayer({
    minzoom: 14,
    query: "node(BBOX)['shop'='farm'];out;",
  });

  var overlayMaps = {
      "Postboxes": L.layerGroup([postboxes]),
      "Toilets": L.layerGroup([toilets]),
      "Hofladen": L.layerGroup([farmshops])
  };
  
  L.control.layers(baseMaps, overlayMaps).addTo(map);
  
  // Geocoder
  var osmGeocoder = new L.Control.OSMGeocoder();
  map.addControl(osmGeocoder);

  // Locate button
  L.control.locate().addTo(map);
});
